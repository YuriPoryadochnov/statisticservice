package com.epam.statisticservice.models;

import javax.persistence.*;

@Entity
@Table(name = "product_statistic")
public class ProductStatistic {
    private long id;
    private String productName;
    private  String username;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
