package com.epam.statisticservice.repositories;

import com.epam.statisticservice.models.ProductStatistic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductStatisticRepository extends JpaRepository<ProductStatistic, Long> {
}
