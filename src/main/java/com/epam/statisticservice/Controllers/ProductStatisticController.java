package com.epam.statisticservice.Controllers;

import com.epam.statisticservice.models.ProductStatistic;
import com.epam.statisticservice.repositories.ProductStatisticRepository;
import com.epam.statisticservice.services.ProductStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductStatisticController implements ProductStatisticService {
    @Autowired
    ProductStatisticRepository productStatisticRepository;

    @Override
    public ProductStatistic view(long id) {
        return productStatisticRepository.findById(id).orElse(null);
    }

    @Override
    public List<ProductStatistic> index() {
        return productStatisticRepository.findAll();
    }

    @Override
    public ProductStatistic create(ProductStatistic productStatistic) {
        return productStatisticRepository.save(productStatistic);
    }
}
