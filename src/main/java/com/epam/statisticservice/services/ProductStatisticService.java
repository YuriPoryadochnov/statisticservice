package com.epam.statisticservice.services;

import com.epam.statisticservice.models.ProductStatistic;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface ProductStatisticService {
    @RequestMapping(value ="/statistic/product/{id}", method = RequestMethod.GET, produces = "application/json")
    public ProductStatistic view(@PathVariable ("id") long id);

    @RequestMapping(value ="/statistic/product", method = RequestMethod.GET, produces = "application/json")
    public List<ProductStatistic> index();

    @RequestMapping(value ="/statistic/product", method = RequestMethod.POST, produces = "application/json")
    public ProductStatistic create(@RequestBody ProductStatistic productStatistic);
}
